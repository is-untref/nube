require 'recaptcha'

module JobVacancy
  class App < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    enable :sessions

    if ENV['SENDGRID_ADDRESS'].nil?
      set :delivery_method, file: {
        location: "#{Padrino.root}/tmp/emails"
      }
    else
      set :delivery_method, smtp: {
        address: ENV['SENDGRID_ADDRESS'],
        port: ENV['SENDGRID_PORT'],
        user_name: ENV['SENDGRID_USERNAME'],
        password: ENV['SENDGRID_PASSWORD'],
        authentication: :plain
      }
    end

    Recaptcha.configure do |config|
      if ENV['SITE_KEY'].nil?
        # Captcha de test de google - https://developers.google.com/recaptcha/docs/faq
        config.site_key = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
        config.secret_key = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
      else
        config.site_key = ENV['SITE_KEY']
        config.secret_key = ENV['SECRET_KEY']
      end
    end
  end
end
