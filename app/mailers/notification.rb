##
# Mailer methods can be defined using the simple format:
#
# email :registration_email do |name, user|
#   from 'admin@site.com'
#   to   user.email
#   subject 'Welcome to the site!'
#   locals  :name => name
#   content_type 'text/html'       # optional, defaults to plain/text
#   via     :sendmail              # optional, to smtp if defined, otherwise sendmail
#   render  'registration_email'
# end
#
# You can set the default delivery settings from your app through:
#
#   set :delivery_method, :smtp => {
#     :address         => 'smtp.yourserver.com',
#     :port            => '25',
#     :user_name       => 'user',
#     :password        => 'pass',
#     :authentication  => :plain, # :plain, :login, :cram_md5, no auth by default
#     :domain          => "localhost.localdomain" # the HELO domain provided
#                         by the client to the server
#   }
#
# or sendmail (default):
#
#   set :delivery_method, :sendmail
#
# or for tests:
#
#   set :delivery_method, :test
#
# or storing emails locally:
#
#   set :delivery_method, :file => {
#     :location => "#{Padrino.root}/tmp/emails",
#   }
#
# and then all delivered mail will use these settings unless otherwise specified.
#

JobVacancy::App.mailer :notification do
  email :contact_info_email do |job_application|
    from 'no_reply@jobvacancy.com'
    to job_application.applicant_email
    subject 'Job Application: Contact information'
    locals job_offer: job_application.job_offer, job_application: job_application
    content_type :plain
    render 'notification/contant_info_email'
  end
end

JobVacancy::App.mailer :notification_to_offerer do
  email :contact_info_email do |job_application|
    from 'no_reply@jobvacancy.com'
    to job_application.job_offer.owner.email
    subject 'Job Application: Contact information'
    locals job_offer: job_application.job_offer, job_application: job_application,
           bio: job_application[:short_bio]
    content_type :plain
    render 'notification/contant_info_email'
  end
end

JobVacancy::App.mailer :recover_password do
  email :email do |recover_password|
    from 'no_reply@jobvacancy.com'
    to recover_password.email
    locals password: recover_password.password
    subject 'Job Application: Password recovery'
    render 'notification/password_recovery_email'
  end
end

JobVacancy::App.mailer :job_question do
  email :email do |job_question|
    from job_question.email
    to job_question.offer.user.email
    locals question: job_question
    subject "[JobVacancy] #{job_question.subject} - #{job_question.offer.title}"
    render 'notification/job_question_email'
  end
end
