JobVacancy::App.controllers :job_question do
  get :question, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_question = JobQuestion.new
    render 'job_question/question'
  end

  post :question, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_question = JobQuestion.new
    if params[:job_question][:email].empty? ||
       params[:job_question][:body].empty? ||
       params[:job_question][:subject].empty?
      flash.now[:error] = 'All fields must be completed!'
      render 'job_question/question'
    elsif !/\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/.match?(params[:job_question][:email])
      flash.now[:error] = 'The email format is invalid!'
      render 'job_question/question'
    else
      @job_question.send_mail(
        params[:job_question][:email],
        params[:job_question][:body],
        params[:job_question][:subject],
        @job_offer
      )
      render 'job_question/question_sent'
    end
  end
end
