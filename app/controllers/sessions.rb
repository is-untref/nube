require 'recaptcha'

JobVacancy::App.controllers :sessions do
  include Recaptcha::ClientHelper
  include Recaptcha::Verify

  get :login, map: '/login' do
    @user = User.new
    render 'sessions/new'
  end

  post :create do
    email = params[:user][:email]
    password = params[:user][:password]
    @user = User.authenticate(email, password)
    login_state = FailedLogin.check_failed_logins(email)
    is_user_blocked = FailedLogin.is_blocked(login_state.id)

    if @user.nil? || is_user_blocked
      @user = User.new
      FailedLogin.login_fail(login_state.id)
      is_user_blocked = FailedLogin.is_blocked(login_state.id)
      if is_user_blocked
        flash.now[:error] = 'Your account is blocked for failed logins!'
      else
        @captcha = true if login_state[:count] >= 2
        flash.now[:error] = 'Invalid credentials'
      end
      render 'sessions/new'
    elsif (login_state[:count] >= 2) && !verify_recaptcha
      @captcha = true
      flash.now[:error] = 'Please, verify the captcha!'
      render 'sessions/new'
    else
      FailedLogin.reset(login_state.id)
      sign_in @user
      redirect '/'
    end
  end

  get :recover do
    render 'sessions/forgot_password'
  end

  get :destroy, map: '/logout' do
    sign_out
    redirect '/'
  end
end
