require 'recaptcha'

JobVacancy::App.controllers :job_offers do
  include Recaptcha::ClientHelper
  include Recaptcha::Verify

  get :my do
    @offers = JobOffer.find_by_owner(current_user)
    render 'job_offers/my_offers'
  end

  get :index do
    @offers = JobOffer.all_active
    render 'job_offers/list'
  end

  get :new do
    @job_offer = JobOffer.new
    render 'job_offers/new'
  end

  get :latest do
    @offers = JobOffer.all_active
    flash.now[:error] = 'Please, log in to apply to an offer.' unless signed_in?
    render 'job_offers/list'
  end

  get :edit, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @tag = JobTag.where(job_offer_id: @job_offer.id).first
    # TODO: validate the current user is the owner of the offer
    render 'job_offers/edit'
  end

  get :apply, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    # redirect 'job_offers/latest' unless signed_in?
    @job_application = JobApplication.new
    offer_tag = JobTag.where(job_offer_id: @job_offer.id).first
    @suggered_offers = []
    unless offer_tag.nil?
      @suggered_offers = JobTag.search_related_job_applications(offer_tag, @job_offer.id)
      # TODO: validate the current user is the owner of the offer
    end
    render 'job_offers/apply'
  end

  get :applications_information, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id].to_i)
    @aplications_information = JobApplication.where(job_offer_id: @job_offer.id)
    render 'job_offers/applications_information'
  end

  post :search do
    @offers = JobOffer.where(Sequel.ilike(:title, "%#{params[:q]}%")\
    | Sequel.ilike(:location, "%#{params[:q]}%") | Sequel.ilike(:description, "%#{params[:q]}%"))
    render 'job_offers/list'
  end

  # rubocop:disable all
  post :apply, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    applicant_email = params[:job_application][:applicant_email]
    @job_application = JobApplication.create_for(applicant_email, @job_offer)
    @job_application.values[:applicant_id] = current_user&.id
    @job_application.values[:job_offer_id] = @job_offer.id
    @job_application.values[:applicant_email] = applicant_email
    @job_application.values[:application_datetime] = Time.now.strftime('%Y-%m-%d %H:%M:%S')
    @job_application.values[:short_bio] = params[:job_application][:short_bio] == '' ?
                                          'No bio' : params[:job_application][:short_bio]
    years_of_experience = params[:job_application]['years_of_experience'].to_i
    @job_application.values[:years_of_experience] = years_of_experience
    offer_tag = JobTag.where(job_offer_id: params[:offer_id]).first
    @suggered_offers = []
    if /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/.match?(applicant_email)
      if verify_recaptcha
        @job_application.save
        @job_application.process
        flash[:success] = 'Contact information sent.'
        redirect '/job_offers'
      else
        flash.now[:error] = 'Please, verify the captcha!'
        unless offer_tag.nil?
          @suggered_offers = JobTag.search_related_job_applications(offer_tag, @job_offer.id)
        end
        render 'job_offers/apply'
      end
    else
      flash.now[:error] = 'The email format is invalid!'
      unless offer_tag.nil?
        @suggered_offers = JobTag.search_related_job_applications(offer_tag, @job_offer.id)
      end
      render 'job_offers/apply'
    end
  end
  # rubocop:enable Metrics/BlockLength

  post :create do
    @job_offer = JobOffer.new
    @job_offer.title = params[:job_offer][:title]
    @job_offer.location = params[:job_offer][:location]
    @job_offer.description = params[:job_offer][:description]
    @job_offer.remuneration = params[:job_offer][:remuneration] == '' ?
                              0 : params[:job_offer][:remuneration]
    @job_offer.owner = current_user
    if @job_offer.save
      unless params[:job_offer][:tag].nil?
        last_id = JobOffer.last.values[:id]
        job_tags = JobTag.new
        job_tags.create(last_id, params[:job_offer][:tag])
        job_tags.save_tags
      end
      TwitterClient.publish(@job_offer) if params['create_and_twit']
      flash[:success] = 'Offer created'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/new'
    end
  end

  post :update, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_offer.title = params[:job_offer][:title]
    @job_offer.location = params[:job_offer][:location]
    @job_offer.description = params[:job_offer][:description]
    @job_offer.remuneration = params[:job_offer][:remuneration] == '' ?
                              0 : params[:job_offer][:remuneration]
    @job_offer.save
    if @job_offer.save
      unless params[:job_offer][:tag].nil?
        if JobTag.where(job_offer_id: params[:offer_id]).first.nil?
          job_tags = JobTag.new
          job_tags.create(params[:offer_id], params[:job_offer][:tag])
          job_tags.save_tags
        else
          job_tags = JobTag.where(job_offer_id: params[:offer_id]).first
          job_tags.edit_tags(params[:offer_id], params[:job_offer][:tag])
        end
      end
      flash[:success] = 'Offer updated'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/edit'
    end
  end

  # rubocop: enable Style/MultilineTernaryOperator
  put :activate, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_offer.activate
    if @job_offer.save
      flash[:success] = 'Offer activated'
    else
      flash.now[:error] = 'Operation failed'
    end

    redirect '/job_offers/my'
  end

  delete :destroy do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    begin
      if @job_offer.destroy
        flash[:success] = 'Offer deleted'
      else
        flash.now[:error] = 'Title is mandatory'
      end
    rescue Sequel::ForeignKeyConstraintViolation
      flash[:error] = 'You cant delete an offer with applicants!'
      redirect 'job_offers/my'
    end
    redirect 'job_offers/my'
  end
end
