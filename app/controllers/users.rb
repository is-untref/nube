JobVacancy::App.controllers :users do
  get :new, map: '/register' do
    @user = User.new
    render 'users/new'
  end

  # rubocop:disable all
  post :create do
    password = params[:user][:password]
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _| k == 'password_confirmation' }
    @user = User.new(params[:user])
    if !@user.validate_password(password)
      flash.now[:error] = 'Invalid password. The password must contain at least 8 characters, a uppercase, a lowercase, a number and a symbol.'
      return render :new
    end

    if !@user.validate_birthdate(params[:user][:birthdate])
      flash.now[:error] = 'The date of birth must be between 18 and 99 years.'
      return render 'users/new'
    end

    if !@user.validate_format_email(params[:user][:email])
      flash.now[:error] = 'Invalid format email'
      return render 'users/new'        
    end

    if !(params[:user][:password] == password_confirmation)
      flash.now[:error] = 'Passwords do not match'
      return render 'users/new'
    end

    if @user.save
      flash[:success] = 'User created'
      return redirect '/'
    else
      flash.now[:error] = 'All fields are mandatory'
      return render 'users/new'
    end
  end
  # rubocop:enable all
end
