JobVacancy::App.controllers :recover_password do
  get :recover_password do
    render 'recover_password'
  end

  get :nonexistent_user do
    render 'nonexistent_user'
  end

  post :sent_password do
    user = User.first(email: params[:recover_password][:email])
    unless user
      redirect 'recover_password/nonexistent_user'
      return
    end
    recovery = RecoverPassword.new
    password = recovery.send_and_generate_password(params[:recover_password][:email])
    user.password = password
    render 'sent_password'
  end
end
