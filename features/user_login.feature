Feature: Add captcha after 3 attempts failed to login
  As user i want than appears a captcha in the following attempts to login after 3 failed attempts

  Background:
    Given no offers, users or applications
    Given user with email "juan@captcha.com" with password "Juan123!"

  Scenario: First failed login doesn't add captcha
    When goes to login page
    And enters his email
  	And enters password "Juan345!"
    And send data
  	Then can't login

  Scenario: Second failed login doesn't add captcha
    When goes to login page
    And enters his email
    And enters password "Juan345!"
  	And send data
  	Then can't login

  Scenario: Third failed login adds captcha
    When goes to login page
    And enters his email
  	And enters password "Juan345!"
    And send data
  	Then can't login and captcha appears
