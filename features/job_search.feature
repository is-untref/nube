Feature: Job Search
  In order to search a job
  I want to get results based on
  title, location or description
  with case insensitive

  Background:
  	Given a "Python Programmer" title, "Buenos Aires" location and "Sr" description offer exists in the offers list
    Given a "python developer" title, "CABA" location and "jr" description offer exists in the offers list

  Scenario: Search offer for title
    When I search for the job "python"
    Then I should obtain 2 results

  Scenario: Search offer for description
    When I search for the job "sr"
    Then I should obtain 1 results

  Scenario: Search offer for location
    When I search for the job "Buenos Aires"
    Then I should obtain 1 results

  Scenario: Search offer for location
    When I search for the job "Java"
    Then I should obtain 0 results

  Scenario: Search offer for location
    When I search for the job "Python Buenos Aires"
    Then I should obtain 0 results


