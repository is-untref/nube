Feature: Add tags to an offer
	As an offerent I want add tags to an offers

	Scenario: Tag added correctly
		Given offerent offerer@test.com loge in,
		  And access the new offer page
		  And fill the title with "Python SR."
		  And tags with "python"
		 When creates the offer
		 Then the offer created with tags "python"

	Scenario: Empty tag
		Given offerent offerer@test.com loge in,
		  And access the new offer page
		  And fill the title with "Python JR."
		  And tags with ""
		 When creates the offer
		 Then the offer created without tag

	Scenario: Save downcase tags
		Given offerent offerer@test.com loge in,
		  And access the new offer page
		  And fill the title with "Programador Java"
		  And tags with "JAVA"
		 When creates the offer
		 Then the offer created with tags "java"

	Scenario: multiple tags
		Given offerent offerer@test.com loge in,
		  And access the new offer page
		  And fill the title with "Programador Java"
		  And tags with "programador,java"
		 When creates the offer
		 Then the offer created with tags "programador,java"

	Scenario: delete repeated tags
		Given offerent offerer@test.com loge in,
		  And access the new offer page
		  And fill the title with "Programador Java"
		  And tags with "programador jr    , java"
		 When creates the offer
		 Then the offer created with tags "programador jr,java"

	Scenario: edit tags
		Given offerent offerer@test.com loge in,
		  And has a offer "Programador COBOL" with tags "programador jr, COBOL"
		 When edit offer changing tags with "Palermo, programador CObol" and save the offer
		 Then the offer save with tags "palermo,programador cobol"