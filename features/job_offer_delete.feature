Feature: Job Offer delete
  In order delete a job offer
  It shouldn't have any application

  Scenario: Delete valid job offer
    Given Im logged in
    And I have a job offer with title "Ruby jr"
    And I access the offers list page
    When I try to delete it
    Then I should see a "Offer deleted" message

  Scenario: Unable to delete a job offer
      Given Im logged in
      And I have a job offer with title "Ruby jr"
      And an unregistered user apply to offer with email "myemail@gmail.com" and 5 years of experience
      And I access the offers list page
      When I try to delete it
      Then I should see a "You cant delete an offer with applicants!" message