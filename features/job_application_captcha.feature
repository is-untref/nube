Feature: Job Application Captcha
  In order apply
  I want the applicant to verify a captcha

  Background:
    Given I am logged in as job offerer

  Scenario: Applicant doesnt check the captcha
    Given job offer with title "Ruby SR"
    And user goes to job offer's list
    And wants to apply to that offer
    And user complete an offer with email "myemail@gmail.com" and 5 years of experience
    When user apply
    Then the user should see "Please, verify the captcha!" message

  Scenario: Applicant checks the captcha
    Given job offer with title "Python ssr"
    And user goes to job offer's list
    And wants to apply to that offer
    And user complete an offer with email "myemail@gmail.com" and 5 years of experience
    And he checks the captcha
    When user apply
    Then the user should see "Contact information sent." message