Feature: Enter new "strong" password
	As a user I want the application
	require a password with a minimum length of 8 characters
	and contain at least one capital letter,
	a lowercase letter, a number
	and a symbol.

	Background:
		Given user enters name "Roberto",
		  And email "roberto@gmail.com" 

	Scenario: Correct password
		Given password "Roberto8!"
		 When send information
		 Then the user be registered

   	Scenario: Incorrect password due to lack of capital
		Given password "roberto88!"
		 When send information
		 Then the user should not be able to register

	Scenario: Incorrect password due to lowercase
		Given password "?JUANMANUEL9"
		 When send information
		 Then the user should not be able to register

	Scenario: Incorrect password due to lack of numbers
		Given password "Tiburon??"
		 When send information
		 Then the user should not be able to register
		
	Scenario: Incorrect password due to lack of symbols
		Given password "123Arb0L5"
		 When send information
		 Then the user should not be able to register

	Scenario: Incorrect password by length 7 characters
		Given password "Hola1*"
		 When send information
		 Then the user should not be able to register