Feature: Job Application Information
 In order to get job applications information
 I want to get the information about all
 applications of my offers

 Background:
  Given no offers, users or applications
  Given user with email "offerer@test.com"
  Given a "Python Programmer" title, "Buenos Aires" location and "Sr" description offer exists in the offers list from first user
  Given user with email "postulante@test.com"

 Scenario: user applies to my offer and i can see his applications information
  When user "postulante@test.com" applies to "Python Programmer" offer
  Then the offerer can see the applications information
