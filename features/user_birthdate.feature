Feature: Request to enter date of birth when registering user
	As an administrator I want when a user registers 
	have to enter the date of birth 

	Background:
		Given A user visits the registration page
		  And complete field name with "Juan", email with "juan@test.com",
		  And the password and the confirmation password with "Juan123!"

	Scenario: Correct registration when enter valid date
		Given complete field date of birth with "01/01/1999"
	     When he try to create the user
         Then the user is created satisfactory

    Scenario: Registration failure because birthdate field is empty
		Given user leave empty birthdate field
		 When he try to create the user
		 Then the user is not created because birtthdate field is mandatory.

    Scenario: Registration failure because enter birthdate less than the valid
		Given complete field date of birth with "15/07/2005"
		 When he try to create the user
		 Then the user is not created because he must be over 18 years old.

    Scenario: Registration failure because enter birthdate greater than valid
		Given complete field date of birth with "15/07/1900"
		 When he try to create the user
		 Then the user is not created because he must be under 99 years old.
