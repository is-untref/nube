Feature: Job Application register
 In order to apply to a job
 The user should be logged in

 Background:
  Given a "Python Programmer" title, "Buenos Aires" location and "Sr" description offer exists in the offers list from first user
  Given user with email "postulante@test.com"

 Scenario: user applies to offer logged in
  Given user with email "postulante@test.com" is logged in
  And user is in the list of offers
  When he tries to apply to the offer
  Then the user can complete the application form

 Scenario: user applies to offer without log in
  Given user is in the list of offers
  When he tries to apply to the offer
  Then he can't apply and a sees the message "Please, log in to apply to an offer."