Given('A user visits the registration page') do
  visit '/register'
  click_button('Create')
end

Given('complete field name with {string}, email with {string},') do |name_introducer, email_introducer|
  fill_in('user[name]', with: name_introducer)
  fill_in('user[email]', with: email_introducer)
end
Given('the password and the confirmation password with {string}') do |password_introducer|
  fill_in('user[password]', with: password_introducer)
  fill_in('user[password_confirmation]', with: password_introducer)
end

Given('complete field date of birth with {string}') do |birthdate_introducer|
  @birthdate = birthdate_introducer
  fill_in('user[birthdate]', with: birthdate_introducer)
end

Given('user leave empty birthdate field') do
  fill_in('user[birthdate]', with: '')
end

When('he try to create the user') do
  click_button('Create')
end

Then('the user is created satisfactory') do
  @birthdate = Date.parse(@birthdate)
  expect(User.last[:birthdate]).to eq @birthdate
end

Then('the user is not created because birtthdate field is mandatory.') do
  page.should have_content('The date of birth must be between 18 and 99 years.')
end

Then('the user is not created because he must be over 18 years old.') do
  page.should have_content('The date of birth must be between 18 and 99 years.')
end

Then('the user is not created because he must be under 99 years old.') do
  page.should have_content('The date of birth must be between 18 and 99 years.')
end
