Given('user with email {string} is logged in') do |email|
  visit '/register'
  fill_in('user[name]', with: 'test')
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: 'Passw0rd!')
  fill_in('user[password_confirmation]', with: 'Passw0rd!')
  click_button('Create')
  visit '/login'
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
end

Given('user is in the list of offers') do
  visit 'job_offers/latest'
end

When('he tries to apply to the offer') do
  pending # Write code here that turns the phrase above into concrete actions
end

Then('the user can complete the application form') do
  click_button('Apply')
  current_path = URI.parse(current_url).path
  page_name = "/job_offers/apply/#{JobOffer.first.id}"
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end

Then("he can't apply and a sees the message {string}") do |message|
  page.should have_content(message)
  page.should have_button('Apply', disabled: true)
end
