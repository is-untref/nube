When(/^I browse the default page$/) do
  visit '/'
end

Given(/^I am logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^I access the new offer page$/) do
  visit '/job_offers/new'
  page.should have_content('Title')
end

When(/^I fill the title with "(.*?)"$/) do |offer_title|
  fill_in('job_offer[title]', with: offer_title)
  @offer_title = offer_title
end

When(/^confirm the new offer$/) do
  click_button('Create')
end

Then(/^I should see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should have_content(content)
end

Then(/^I should not see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

And('fill the location with {string}') do |location|
  fill_in('job_offer[location]', with: location)
  @location = location
end

And('fill the description with {string}') do |description|
  fill_in('job_offer[description]', with: description)
  @description = description
end

And('fill the remuneration with {int}') do |remuneration|
  fill_in('job_offer[remuneration]', with: remuneration)
  @remuneration = remuneration
end

And('fill the tags with {string}') do |tags|
  fill_in('job_offer[tag]', with: tags)
  @tags = tags
end

Given(/^I have "(.*?)" offer in My Offers$/) do |offer_title|
  JobApplication.all.each(&:delete)
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: offer_title)
  click_button('Create')
end

Given(/^I edit it$/) do
  click_link('Edit')
end

And(/^I delete it$/) do
  click_button('Delete')
end

And('I should have a offer with that information') do
  last_offer = JobOffer.last
  expect(last_offer.location).to eq @location
  expect(last_offer.title).to eq @offer_title
  expect(last_offer.description).to eq @description
  expect(last_offer.remuneration).to eq @remuneration
  expect(JobTag.last.name).to eq @tags
end

And('I should have a offer with that information and {int} in remuneration') do |remuneration|
  last_offer = JobOffer.last
  expect(last_offer.location).to eq @location
  expect(last_offer.title).to eq @offer_title
  expect(last_offer.description).to eq @description
  expect(last_offer.remuneration).to eq remuneration
  expect(JobTag.last.name).to eq @tags
end

Given(/^I set title to "(.*?)"$/) do |new_title|
  fill_in('job_offer[title]', with: new_title)
end

Given(/^I save the modification$/) do
  click_button('Save')
end
