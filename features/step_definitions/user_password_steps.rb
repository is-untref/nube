Given('user enters name {string},') do |enter_name|
  @name = enter_name
end

Given('email {string}') do |enter_email|
  @email = enter_email
end

Given('password {string}') do |enter_password|
  @password = enter_password
end

When('send information') do
  visit '/register'
  fill_in('user[name]', with: @name)
  fill_in('user[email]', with: @email)
  fill_in('user[birthdate]', with: '24/12/1992')
  fill_in('user[password]', with: @password)
  fill_in('user[password_confirmation]', with: @password)
  click_button('Create')
end

Then('the user be registered') do
  page.should have_content('User created')
end

Then('the user should not be able to register') do
  # rubocop:disable LineLength
  page.should have_content('Invalid password. The password must contain at least 8 characters, a uppercase, a lowercase, a number and a symbol.')
  # rubocop:enable LineLength
end
