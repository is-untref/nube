Given('no offers, tags or applications') do
  JobApplication.all.each(&:delete)
  JobOffer.all.each(&:delete)
  JobTag.all.each(&:delete)
end

Given('a {string} offer with tags {string}') do |title, tags|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.location = 'some location'
  @job_offer.description = 'some description'
  @job_offer.save
  @tag = JobTag.new
  @tag[:job_offer_id] = @job_offer.id
  @tag[:name] = tags.downcase
  @tag.save
end

When('user goes to offers list') do
  visit '/job_offers/latest'
end

When('applies to {string} offer') do |string|
  job_offer = JobOffer.where(title: string).first
  find("##{job_offer.id}").click
end

Then('can see suggestion for {string} and {string}') do |string, string2|
  page.should have_content(string)
  page.should have_content(string2)
end

Then('cant see suggested offers') do
  page.should have_no_content('Some other related offers that might interest to you')
end

Then('can see {string}') do |string|
  page.should have_content(string)
end

And('see {string}') do |string|
  page.should have_content(string)
end

And('not see {string}') do |string|
  page.should have_no_content(string)
end
