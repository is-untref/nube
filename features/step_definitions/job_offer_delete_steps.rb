Given('Im logged in') do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
end

Given(/^I have a job offer with title "([^"]*)"$/) do |title|
  JobApplication.all.each(&:delete)
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: title)
  click_button('Create')
end

Given('an unregistered user apply to offer with email {string} and {int} years of experience') do |email, years|
  visit "/job_offers/apply/#{JobOffer.first.id}"
  fill_in('job_application[applicant_email]', with: email)
  fill_in('job_application[years_of_experience]', with: years)
  click_button('Apply')
end

When('I try to delete it') do
  visit '/job_offers/my'
  click_button('Delete')
end

Then(/^I should see a "([^"]*)" message$/) do |content|
  page.should have_content(content)
end
