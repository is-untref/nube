Given('a {string} title, {string} location and {string} description offer exists in the offers list') do |title, location, description|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.location = location
  @job_offer.description = description
  @job_offer.save
end

When('I search for the job {string}') do |job|
  visit '/job_offers'
  fill_in('q', with: job)
  click_button('search')
end

Then('I should obtain {int} results') do |results_quantity|
  page.should have_css('table tr', count: results_quantity + 1)
  JobOffer.all.each(&:delete)
end
