Given('user with email {string} and password {string}') do |email, password|
  visit '/register'
  @email = email
  @password = password
  fill_in('user[name]', with: 'Test')
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
  fill_in('user[password_confirmation]', with: password)
  click_button('Create')
end

Given('he tried to login incorrectly {int} times already') do |attempts|
  visit '/login'
  (0..attempts).each do
    fill_in('user[email]', with: @email)
    fill_in('user[password]', with: 'incorrectpassword')
    click_button('Login')
  end
end

When('he goes to login site') do
  visit '/login'
end

When('enters mail {string} and password {string}') do |email, password|
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
end

When('tries to log in') do
  click_button('Login')
end

Then("can't login and {string} appears") do |message|
  page.should have_content(message)
end

Then('{string} account is now blocked') do |email|
  expect(FailedLogin.is_blocked(FailedLogin.check_failed_logins(email).id)).to be_truthy
end
