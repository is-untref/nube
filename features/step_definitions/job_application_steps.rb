Given('a user created') do
  visit '/register'
  fill_in('user[name]', with: 'test')
  fill_in('user[email]', with: 'test2@email.com')
  fill_in('user[birthdate]', with: '24/12/1992')
  fill_in('user[password]', with: 'Password2!')
  fill_in('user[password_confirmation]', with: 'Password2!')
  click_button('Create')
end

Given(/^only a "(.*?)" offer exists in the offers list$/) do |job_title|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = job_title
  @job_offer.location = 'a nice job'
  @job_offer.description = 'a nice job'
  @job_offer.save
end

Given(/^I access the offers list page$/) do
  visit '/job_offers'
end

And('registered user with email {string} and password {string}') do |string, password|
  visit '/register'
  fill_in('user[name]', with: 'Alejandro Lopez 21')
  fill_in('user[birthdate]', with: '24/11/1995')
  fill_in('user[email]', with: string)
  fill_in('user[password]', with: password)
  fill_in('user[password_confirmation]', with: password)
  click_button('Create')
end

Given('job_offer with title {string}') do |string|
  @job_offer = JobOffer.new
  @job_offer.title = string
  @job_offer.location = 'some location'
  @job_offer.description = 'some description'
  @job_offer.owner = User.first
  @job_offer.save
end

When('I apply with email {string}') do |string|
  visit '/job_offers/latest'
  find("##{@job_offer.id}").click
  fill_in('job_application[applicant_email]', with: string)
  click_button('Apply')
end

When('user logs in with email {string} and password {string}') do |email, password|
  visit '/login'
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
  click_button('Login')
end

When("goes to job offer's list") do
  visit '/job_offers'
end

When('applies to offer with bio {string}') do |bio|
  @last_job_offer = JobOffer.last
  find("##{@last_job_offer.id}").click
  fill_in('job_application[applicant_email]', with: User.last.email)
  fill_in('job_application[years_of_experience]', with: 5)
  fill_in('job_application[short_bio]', with: bio)
  click_button('Apply')
end

When('applies to offer with no bio') do
  @last_job_offer = JobOffer.last
  find("##{@last_job_offer.id}").click
  p User.last.email
  fill_in('job_application[applicant_email]', with: User.last.email)
  fill_in('job_application[years_of_experience]', with: 5)
  click_button('Apply')
end

And('applies to offer with {int} years of experience in offers technology') do |years|
  @last_job_offer = JobOffer.last
  find("##{@last_job_offer.id}").click
  fill_in('job_application[applicant_email]', with: User.last.email)
  fill_in('job_application[years_of_experience]', with: years)
  click_button('Apply')
end

Then("system saves application with {int} years of experience of the applicant in the offer's technology") do |int|
  last_job_application = JobApplication.last
  expect(last_job_application.years_of_experience).to eq int
end

Then('system saves job_application with email, date and time') do
  expect(@datetime).to eq JobApplication.last.application_datetime.strftime('%Y-%m-%d %H:%M:%S')
end

Then(/^I should receive a mail with offerer info$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
  JobApplication.all.each(&:delete)
  JobOffer.all.each(&:delete)
end

Then('system saves application with bio {string}') do |bio|
  last_job_application = JobApplication.last
  expect(last_job_application[:short_bio]).to eq bio
end

Then('the user cant´t apply to the offer because de mail is invalid.') do
  page.should have_content('The email format is invalid!')
end

When('user apply to offer with email {string}') do |string|
  @last_job_offer = JobOffer.last
  find("##{@last_job_offer.id}").click
  fill_in('job_application[applicant_email]', with: string)
  click_button('Apply')
  @datetime = Time.now.strftime('%Y-%m-%d %H:%M:%S')
end

Then('the user cant apply to the offer.') do
  page.should have_content('Contact information sent.')
end
