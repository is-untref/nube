Given('no offers, users or applications') do
  JobApplication.all.each(&:delete)
  JobOffer.all.each(&:delete)
  User.all.each(&:delete)
end

Given('user with email {string}') do |string|
  @name = 'offerer'
  @email = string
  @password = 'Passw0rd!'
  visit '/register'
  fill_in('user[name]', with: @name)
  fill_in('user[birthdate]', with: '24/11/1992')
  fill_in('user[email]', with: @email)
  fill_in('user[password]', with: @password)
  fill_in('user[password_confirmation]', with: @password)
  click_button('Create')
end

Given('a {string} title, {string} location and {string} description offer exists in the offers list from first user') do |string, string2, string3|
  @job_offer = JobOffer.new(title: string, description: string3, location: string2)
  @job_offer.owner = User.where(email: @email).first
  @job_offer.save
end

When('user {string} applies to {string} offer') do |string1, string2|
  @applicant = User.where(email: string1).first
  @job_offer = JobOffer.where(title: string2).first
  @job_application = JobApplication.new(applicant_id: @applicant.id,
                                        job_offer_id: @job_offer.id,
                                        years_of_experience: 4,
                                        application_datetime: Time.now
                                        .strftime('%Y-%m-%d %H:%M:%S'))
  @job_application[:applicant_email] = @applicant.email
  @job_application.save
end

Then('the offerer can see the applications information') do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  job_offer_id = @job_offer.id
  job_application = JobApplication.last
  visit("/job_offers/applications_information/#{job_offer_id}")
  page.should have_content(job_application.applicant_email)
  page.should have_content(job_application.application_datetime.strftime('%d/%m/%Y'))
  page.should have_content(job_application.years_of_experience)
end
