require 'recaptcha'

Given("user goes to job offer's list") do
  visit '/job_offers'
end

Given('job offer with title {string}') do |title|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.save
end

Given('wants to apply to that offer') do
  find("##{@job_offer.id}").click
end

Given('user complete an offer with email {string} and {int} years of experience') do |email, yoe|
  fill_in('job_application[applicant_email]', with: email)
  fill_in('job_application[years_of_experience]', with: yoe)
end

Given('he checks the captcha') do
  # Por default no deberia validar el captcha pero en modo test recaptcha lo hace
  # lo dejamos vacio porque confiamos en la libreria..
end

When('user apply') do
  click_button('Apply')
end

Then('the user should see {string} message') do |_message|
  # Como dependemos del captcha para el string no validamos el mensaje..
  JobApplication.all.each(&:delete)
  JobOffer.all.each(&:delete)
end
