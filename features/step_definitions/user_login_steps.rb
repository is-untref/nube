Given('user with email {string} with password {string}') do |string, string2|
  @name = 'offerer'
  @email = string
  @password = string2
  visit '/register'
  fill_in('user[name]', with: @name)
  fill_in('user[email]', with: @email)
  fill_in('user[password]', with: @password)
  fill_in('user[password_confirmation]', with: @password)
  click_button('Create')
  visit('/logout')
end

When('goes to login page') do
  visit('/login')
end

When('enters his email') do
  fill_in('user[email]', with: @email)
end

When('enters password {string}') do |string|
  fill_in('user[password]', with: string)
end

When('send data') do
  click_button('Login')
end

Then("can't login") do
  page.should have_content('Invalid credentials')
end

Then("can't login and captcha appears") do
  page.should have_css('#captcha')
  FailedLogin.all.each(&:delete)
end
