Given('offerent offerer@test.com loge in,') do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given('access the new offer page') do
  visit '/job_offers/new'
  page.should have_content('Title')
  page.should have_content('Tag')
end

Given('fill the title with {string}') do |title_introduced|
  @title = title_introduced
end

Given('tags with {string}') do |tag_introduced|
  @tag = tag_introduced
end

When('creates the offer') do
  fill_in('job_offer[title]', with: @title)
  fill_in('job_offer[tag]', with: @tag)
  click_button('Create')
end

Then('the offer created with tags {string}') do |tags_saved|
  expect(tags_saved).to eq JobTag.last.values[:name]
  JobTag.all.each(&:delete)
end

Then('the offer created without tag') do
  expect(JobTag.last[:name]).to eq ''
  JobTag.all.each(&:delete)
end

Then('the offer created with tags {string} y {string}') do |tag1, tag2|
  job_tags = JobTag.all
  expect(tag1).to eq job_tags[0].values[:name]
  expect(tag2).to eq job_tags[1].values[:name]
  JobTag.all.each(&:delete)
end

Given('has a offer {string} with tags {string}') do |title, tags|
  @job_offer = JobOffer.new
  @job_offer.title = title
  @job_offer.description = 'some description'
  @job_offer.owner = User.first
  @job_offer.save
  job_tags = JobTag.new
  job_tags.create(@job_offer.id, tags)
  job_tags.save_tags
end

When('edit offer changing tags with {string} and save the offer') do |new_tags|
  visit 'job_offers/edit/' + @job_offer.id.to_s
  fill_in('job_offer[tag]', with: new_tags)
  click_button('Save')
end

Then('the offer save with tags {string}') do |tags|
  expect(JobTag.last[:name]).to eq tags
  JobTag.all.each(&:delete)
end
