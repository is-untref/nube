Given('the offer {string} and user that want to ask a question') do |offer|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = offer
  @job_offer.save
end

Given('the mail {string}, subject {string}, question {string}') do |mail, subject, question|
  id = @job_offer.id.to_s
  visit "job_question/question/#{id}"
  @email = mail
  @question = question
  fill_in('job_question[email]', with: mail)
  fill_in('job_question[subject]', with: subject)
  fill_in('job_question[body]', with: question)
end

When('user sends the information,') do
  click_button('Send')
end

Then('the offerer receives an email with Body {string} and subject {string}') do |body, subject|
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/#{@job_offer.owner.email}", 'r')
  content = file.read
  content.include?(body).should be true
  content.include?(subject).should be true
  JobOffer.all.each(&:delete)
end

Then("the user couldn't send the mail due to empty information") do
  page.should have_content('All fields must be completed!')
end

Then("the user couldn't send the question due to invalid email") do
  page.should have_content('The email format is invalid!')
end
