Feature: Block account after 6 attempts failed to login

  Scenario: Sixth failed login blocks account
    Given user with email "juan@blocked.com" and password "Juan123!"
    And he tried to login incorrectly 5 times already
    When he goes to login site
    And enters mail "juan@blocked.com" and password "Juan345!"
    And tries to log in
    Then can't login and "Your account is blocked for failed logins!" appears
    And "juan@blocked.com" account is now blocked