Feature: Asking question to offerer
    In order to ask questions
    As a user
    I want to make question to offerers about offers

  Background:
    Given the offer "Ruby Jr" and user that want to ask a question

  Scenario: Email correctly received by offerer
      Given the mail "agustin@jobvacancy.com", subject "Time", question "Until what time is the job?"
      When user sends the information,
      Then the offerer receives an email with Body "agustin@jobvacancy.com wants to know: Until what time is the job?" and subject "[JobVacancy] Time - Ruby Jr"

  Scenario: Couldn't ask question due to empty mail
      Given the mail "", subject "Benefits", question "Is social work offered as a benefit?"
      When user sends the information,
      Then the user couldn't send the mail due to empty information

  Scenario: Couldn't ask question due to empty body.
      Given the mail "user@gmail.com", subject "Benefits", question ""
      When user sends the information,
      Then the user couldn't send the mail due to empty information

  Scenario: Couldn't ask question due invalid email.
      Given the mail "invalid_email", subject "Benefits", question "Is social work offered as a benefit?"
      When user sends the information,
      Then the user couldn't send the question due to invalid email