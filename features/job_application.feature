Feature: Job Application
  In order to get a job
  As a candidate
  I want to apply to an offer

  Background:
    Given only a "Web Programmer" offer exists in the offers list
    Given I am logged in as job offerer

  Scenario: Apply to job offer
    Given I access the offers list page
    When I apply with email "applicant@test.com"
    Then I should receive a mail with offerer info

  Scenario: Applicant puts his/her years experience in job's technology
    Given job_offer with title "Programador Python SSR para desarrollo de aplicación con inteligencia artificial"
    And registered user with email "Alejandro-Lopez1@gmail.com" and password "Alejandro-Lopez1"
    When user logs in with email "Alejandro-Lopez1@gmail.com" and password "Alejandro-Lopez1"
    And goes to job offer's list
    And applies to offer with 3 years of experience in offers technology
    Then system saves application with 3 years of experience of the applicant in the offer's technology

    Scenario: Applicant puts his/her bio
      Given job_offer with title "Programador Php SR para desarrollo de aplicación con Laravel"
      And registered user with email "Pedro-Lopez1@gmail.com" and password "Pedro-Lopez1"
      When user logs in with email "Pedro-Lopez1@gmail.com" and password "Pedro-Lopez1"
      And goes to job offer's list
      And applies to offer with bio "biografia de pedro lopez 1"
      Then system saves application with bio "biografia de pedro lopez 1"

    Scenario: Applicant doesn't put his/her bio
      Given job_offer with title "Programador Php SR para desarrollo de aplicación con Laravel"
      And registered user with email "Pedro-Lopez1@gmail.com" and password "Pedro-Lopez1"
      When user logs in with email "Pedro-Lopez1@gmail.com" and password "Pedro-Lopez1"
      And goes to job offer's list
      And applies to offer with no bio
      Then system saves application with bio "No bio"

    Scenario: Applicant doesn't put his/her bio # NO automatizado, realizar a mano
      Given job_offer with title "Programador Php SR para desarrollo de aplicación con Laravel"
      When user goes to that offer application's
      And tries to apply with correct information but with a bio larger than 500 characters
      Then system doesn't let him put that bio

  Scenario: Applicant puts an invalid email
    Given job_offer with title "Programador Python SSR para desarrollo de aplicación con inteligencia artificial"
    And goes to job offer's list
    When user apply to offer with email "invalid_email"
    Then the user cant´t apply to the offer because de mail is invalid.

  Scenario: Apply to job offer
    Given job_offer with title "Programador Python SSR para desarrollo de aplicación con inteligencia artificial"
    And goes to job offer's list
    When user apply to offer with email "valid@test.com"
    Then the user cant apply to the offer.
