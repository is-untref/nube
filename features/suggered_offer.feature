Feature: See suggested offers similar than whose i'm applying

  Background:
    Given I am logged in as job offerer
    Given no offers, tags or applications
    Given a "Programador Python ssr con experiencia en Django" offer with tags "python,mvc,django"
    Given a "Programador Java ssr con experiencia en Spring 4" offer with tags "java,mvc,spring,ssr"
    Given a "Programador Php SR con experiencia en Laravel o Symfony" offer with tags "php,laravel,symfony,sr,mvc"
    Given a "Programador Ruby SR con experiencia en Rails" offer with tags "ruby,mvc,rails,sr"
    Given a "Programador C jr" offer with tags "jr"
    Given a "Programador python o ruby con frameworks mvc" offer with tags "python,ruby,mvc,frameworks"

  Scenario: See similar offers that whose user is applying
    When user goes to offers list
    And applies to "Programador Php SR con experiencia en Laravel o Symfony" offer
    Then can see suggestion for "Programador Ruby SR con experiencia en Rails" and "Programador Java ssr con experiencia en Spring 4"

  Scenario: Offer doesnt have similar offers to show
    When user goes to offers list
    And applies to "Programador C jr" offer
    Then cant see suggested offers

  Scenario: User see max count of suggested offers
    When user goes to offers list
    And applies to "Programador Python ssr con experiencia en Django" offer
    Then can see "Programador Java ssr con experiencia en Spring 4"
    And see "Programador Php SR con experiencia en Laravel o Symfony"
    And see "Programador python o ruby con frameworks mvc"
    And not see "Programador Ruby SR con experiencia en Rails"
