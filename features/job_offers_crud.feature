Feature: Job Offers CRUD
  In order to get employees
  As a job offerer
  I want to manage my offers

  Background:
    Given I am logged in as job offerer

  Scenario: Create new offer
    Given I access the new offer page
    When I fill the title with "Programmer vacancy"
    And fill the location with "Saavedra"
    And fill the description with "Description of this job offer"
    And fill the remuneration with 25000
    And fill the tags with "programer,vacancy,javascript,php,java,c#"
    And confirm the new offer
    Then I should see "Offer created"
    And I should see "Programmer vacancy" in My Offers
    And I should have a offer with that information

  Scenario: Create new offer
    Given I access the new offer page
    When I fill the title with "Java developer ssr"
    And fill the location with "Nuniez"
    And fill the description with "java developer with knowdledge of spring core"
    And fill the tags with "java,mvc,spring,spring core,ssr"
    And confirm the new offer
    Then I should see "Offer created"
    And I should see "Java developer ssr" in My Offers
    And I should see "No specified" in My Offers
    And I should have a offer with that information and 0 in remuneration

  Scenario: Update offer
    Given I have "Programmer vacancy" offer in My Offers
    And I edit it
    And I set title to "Programmer vacancy!!!"
    And I save the modification
    Then I should see "Offer updated"
    And I should see "Programmer vacancy!!!" in My Offers

  Scenario: Delete offer
    Given I have "Programmer vacancy" offer in My Offers
    Given I delete it
    Then I should see "Offer deleted"
    And I should not see "Programmer vacancy!!!" in My Offers
