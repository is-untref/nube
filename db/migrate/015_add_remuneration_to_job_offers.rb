Sequel.migration do
  up do
    alter_table(:job_offers) do
      add_column :remuneration, Integer, null: true
    end
  end

  down do
    alter_table(:job_offers) do
      drop_column(:remuneration)
    end
  end
end
