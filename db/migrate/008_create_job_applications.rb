Sequel.migration do
  up do
    create_table(:job_applications) do
      primary_key :id
      foreign_key :applicant_id, :users, null: true
      foreign_key :job_offer_id, :job_offers, null: false, on_delete: :cascade, on_update: :cascade
    end
  end

  down do
    drop_table(:job_applications)
  end
end
