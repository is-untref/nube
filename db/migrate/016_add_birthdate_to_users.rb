Sequel.migration do
  up do
    alter_table(:users) do
      add_column :birthdate, Date, null: true
    end
  end

  down do
    alter_table(:users) do
      drop_column(:birthdate)
    end
  end
end
