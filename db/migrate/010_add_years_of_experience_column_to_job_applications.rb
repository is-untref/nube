Sequel.migration do
  up do
    alter_table(:job_applications) do
      add_column :years_of_experience, Integer, null: false
    end
  end

  down do
    alter_table(:job_applications) do
      drop_column(:years_of_experience)
    end
  end
end
