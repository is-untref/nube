Sequel.migration do
  up do
    create_table(:failed_logins) do
      primary_key :id
      String :email, null: false, unique: true
      Integer :count
      Boolean :block
      DateTime :updated_at
    end
  end

  down do
    drop_table(:failed_logins)
  end
end
