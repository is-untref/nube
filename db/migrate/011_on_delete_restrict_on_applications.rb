Sequel.migration do
  up do
    alter_table(:job_applications) do
      drop_foreign_key [:job_offer_id], name: :job_applications_job_offer_id_fkey
      add_foreign_key [:job_offer_id], :job_offers, on_delete: :restrict,
                                                    name: :job_applications_job_offer_id_fkey
    end
  end

  down do
    alter_table(:job_applications) do
      drop_foreign_key [:job_offer_id], name: :job_applications_job_offer_id_fkey
      add_foreign_key [:job_offer_id], :job_offers, on_delete: :cascade,
                                                    name: :job_applications_job_offer_id_fkey
    end
  end
end
