Sequel.migration do
  up do
    alter_table(:job_applications) do
      add_column :short_bio, String, null: true, max: 500
    end
  end

  down do
    alter_table(:job_applications) do
      drop_column(:short_bio)
    end
  end
end
