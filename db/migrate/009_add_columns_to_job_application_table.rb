Sequel.migration do
  up do
    alter_table(:job_applications) do
      add_column :applicant_email, String, null: true
      add_column :application_datetime, DateTime, null: true
    end
  end

  down do
    alter_table(:job_applications) do
      drop_column(:applicant_email)
      drop_column(:application_datetime)
    end
  end
end
