Sequel.migration do
  up do
    create_table(:job_tags) do
      primary_key :id
      foreign_key :job_offer_id, :job_offers, null: false, on_delete: :cascade, on_update: :cascade
      String :name
    end
  end

  down do
    drop_table(:job_tags)
  end
end
