class JobApplication < Sequel::Model
  attr_accessor :applicant_email
  attr_accessor :applicant_id
  attr_accessor :job_offer
  attr_accessor :short_bio
  one_to_one :users
  one_to_one :job_offers

  def self.create_for(email, offer)
    app = JobApplication.new
    app.applicant_email = email
    app.job_offer = offer
    app
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
    JobVacancy::App.deliver(:notification_to_offerer, :contact_info_email, self)
  end
end
