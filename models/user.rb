class User < Sequel::Model
  one_to_many :job_offers
  one_to_many :job_applications

  def validate
    super
    validates_presence %i[name email crypted_password birthdate]
    validates_format(/\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/, :email)
  end

  def password=(password)
    return unless validate_password(password)

    self.crypted_password = ::BCrypt::Password.create(password) unless password.nil?
  end

  def validate_format_email(email)
    validate_email = true
    validate_email = false unless email.match?(/\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/)
    validate_email
  end

  def self.authenticate(email, password)
    user = User.first(email: email)
    user&.has_password?(password) ? user : nil
  end

  def has_password?(password)
    ::BCrypt::Password.new(crypted_password) == password
  end

  def validate_password(pass)
    val_pass = true
    val_pass = false unless pass.match?(obtain_password_regex)
    val_pass
  end

  def validate_birthdate(birthdate)
    if birthdate != ''
      birthdate = Date.parse(birthdate)
      age = (DateTime.now - birthdate) / 365.25
      valid_age = false
      valid_age = true if age >= 18 && age <= 99
    end

    valid_age
  end

  def obtain_password_regex
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}/
  end
end
