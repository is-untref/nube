class RecoverPassword
  attr_accessor :email
  attr_accessor :password

  def generate_password(email)
    # TODO: try to generate the password randomly
    # password = user.obtain_password_regex.generate
    self.email = email
    self.password = 'new$Password123'
  end

  def send_password
    JobVacancy::App.deliver(:recover_password, :email, self)
  end

  def send_and_generate_password(email)
    password = generate_password(email)
    send_password
    password
  end
end
