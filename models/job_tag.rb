class JobTag < Sequel::Model
  # rubocop:disable all
  attr_accessor :tags, :job_offer_id, :chain_of_tags

  def create(job_offer_id, chain_of_tags)
    @job_offer_id = job_offer_id
    @chain_of_tags = chain_of_tags.downcase
  end

  def save_tags
    @job_tag = JobTag.new
    @job_tag.values[:job_offer_id] = @job_offer_id
    if @chain_of_tags == ""
      @job_tag.values[:name] = @chain_of_tags
    else
      @job_tag.values[:name] = delete_repeated_tags(@chain_of_tags)
    end
    @job_tag.save
  end

  def delete_spaces(word)
    if word.start_with?(' ')
      word = word[1..-1]
      delete_spaces(word)
    end

    if word.end_with?(' ')
      word = word[0..-2]
      delete_spaces(word)
    else
      return word
    end
  end

  def edit_tags(job_offer_id, new_tags)
    job_tag = JobTag.where(job_offer_id: job_offer_id).first
    tags = delete_repeated_tags(new_tags.downcase)
    job_tag.update(name: tags)
  end

  def delete_repeated_tags(chain_of_tags)
    initial_array = chain_of_tags.split(",")
    final_array = []
    initial_array.each do |separated_tag|

      final_array.push(delete_spaces(separated_tag)) if (final_array.include?(separated_tag) == false)
    end
    string = final_array[0]
    for i in 1...final_array.count do
      string << ",#{final_array[i]}"
    end
    string
  end

  def self.search_related_job_applications(offer_tag, this_job_offer_id)
    tag_name_array = offer_tag.name.split(',')
    @suggested_job_offers = []
    tag_name_array.each do |tag_name|
      offers_to_include = JobOffer.fetch("SELECT job_offers.* FROM job_offers, job_tags where job_offers.id = job_tags.job_offer_id AND job_tags.name LIKE '%#{tag_name}%'").all
      offers_to_include.each do |job_offer|
        @suggested_job_offers.push(job_offer) if ((@suggested_job_offers.include? job_offer) == false) && (job_offer.id != this_job_offer_id)
      end
    end
    @suggested_job_offers.take(3)
  end
end
