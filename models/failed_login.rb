class FailedLogin < Sequel::Model
  attr_accessor :email
  attr_accessor :count
  attr_accessor :block
  attr_accessor :updated_at

  def self.check_failed_logins(email)
    @login_state = FailedLogin.where(email: email).first
    if @login_state.nil?
      @login_state = FailedLogin.new
      @login_state[:email] = email
      @login_state[:count] = 0
      @login_state[:block] = false
      @login_state[:updated_at] = Time.now
      @login_state.save
      FailedLogin.where(email: email).first
    end

    @login_state
  end

  def self.is_blocked(id)
    login_state_plus = FailedLogin.where(id: id).first
    login_state_plus[:count] >= 6
  end

  def self.login_fail(id)
    login_state_plus = FailedLogin.where(id: id).first
    login_state_plus[:count] = login_state_plus[:count].to_i + 1
    login_state_plus.save
  end

  def self.reset(id)
    login_state_plus = FailedLogin.where(id: id).first
    login_state_plus[:count] = 0
    login_state_plus.save
  end
end
