class JobQuestion
  attr_accessor :email
  attr_accessor :body
  attr_accessor :subject
  attr_accessor :offer

  def initialize
    @email = nil
    @body = nil
    @subject = nil
    @offer = nil
  end

  def send_mail(email, body, subject, offer)
    self.email = email
    self.body = body
    self.subject = subject
    self.offer = offer
    JobVacancy::App.deliver(:job_question, :email, self)
  end
end
