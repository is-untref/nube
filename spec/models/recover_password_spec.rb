require 'spec_helper'

describe RecoverPassword do
  describe 'generate_password' do
    it 'should generate a random valid password' do
      email = 'perdimipassword@test.com'
      recovery = described_class.new
      recovery.generate_password(email)
      expect(recovery.email).not_to be_nil
      expect(recovery.password).not_to be_nil
    end
  end

  describe 'send_password' do
    it 'should send the recovery email' do
      email = 'perdimipassword@test.com'
      recovery = described_class.new
      expect(JobVacancy::App).to receive(:deliver).with(:recover_password, :email, recovery)
      recovery.send_and_generate_password(email)
    end
  end
end
