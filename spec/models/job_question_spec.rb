require 'spec_helper'

describe JobQuestion do
  describe 'send_mail' do
    it 'should send the question email' do
      email = 'question@test.com'
      question = described_class.new
      expect(JobVacancy::App).to receive(:deliver).with(:job_question, :email, question)
      question.send_mail(email, 'body', 'subject', JobOffer.new)
    end
  end
end
