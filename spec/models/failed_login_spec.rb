require 'spec_helper'

describe FailedLogin do
  let(:failed_login) { described_class.new }

  describe 'model' do
    it { is_expected.to respond_to(:email) }
    it { is_expected.to respond_to(:count) }
    it { is_expected.to respond_to(:block) }
    it { is_expected.to respond_to(:updated_at) }
  end

  describe '::check_failed_logins' do
    let(:instance) { described_class.check_failed_logins('email_que_no_existe@gmail.com') }

    before(:each) do
      described_class.all.each(&:delete)
    end

    it 'should email be email_que_no_existe@gmail.com' do
      expect(instance[:email]).to eq 'email_que_no_existe@gmail.com'
    end

    it 'should count be zero' do
      expect(instance[:count]).to eq 0
    end

    it 'should not be blocked initally ' do
      expect(instance[:block]).to eq false
    end
  end

  describe '::login_fail' do
    let(:instance) { described_class.check_failed_logins('email_que_no_existe@gmail.com') }

    before(:each) do
      described_class.all.each(&:delete)
    end

    it 'count should add 1' do
      expect(described_class.login_fail(instance.id)[:count]).to eq 1
    end
  end

  describe 'is_blocked' do
    let(:instance) { described_class.check_failed_logins('juan@blocked.com') }

    before(:each) do
      described_class.all.each(&:delete)
    end

    it 'should return true due to many attempts' do
      instance[:count] = 6
      instance.save
      expect(described_class.is_blocked(instance.id)).to be_truthy
    end
    it 'should return false due to not many attempts' do
      instance[:count] = 5
      instance.save
      expect(described_class.is_blocked(instance.id)).to be_falsey
    end
  end
end
