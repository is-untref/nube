require 'spec_helper'

describe JobTag do
  let(:job_tag) { described_class.new }

  describe 'model' do
    it { is_expected.to respond_to(:tags) }
    it { is_expected.to respond_to(:job_offer_id) }
    it { is_expected.to respond_to(:create) }
    it { is_expected.to respond_to(:save_tags) }
    it { is_expected.to respond_to(:delete_spaces) }
    it { described_class.should respond_to(:search_related_job_applications) }
  end

  describe 'delete spaces' do
    it 'must return "RUBY" when introduce tag " RUBY"' do
      tag = 'RUBY'
      expect(job_tag.delete_spaces(' RUBY')).to eq tag
    end

    it 'must return "Java" when introduce tag "Java "' do
      tag = 'Java'
      expect(job_tag.delete_spaces('Java ')).to eq tag
    end

    it 'must return "python" when introduce tag " python "' do
      tag = 'python'
      expect(job_tag.delete_spaces(' python ')).to eq tag
    end

    it 'must return "desarrollador jr" when introduce tag " desarrollador jr "' do
      tag = 'desarrollador jr'
      expect(job_tag.delete_spaces(' desarrollador jr ')).to eq tag
    end

    it 'must return "  C#  " when introduce tag "  C#  "' do
      tag = 'C#'
      expect(job_tag.delete_spaces('  C#  ')).to eq tag
    end
  end

  describe 'tag downcase' do
    it 'must return "ruby" when introduce tag "RUBY"' do
      tag = 'ruby'
      job_tag.create(0, 'RUBY')
      expect(job_tag.chain_of_tags).to eq tag
    end

    it 'must return "ruby, dev jr" when introduce tag "RUBY, Dev JR"' do
      tag = 'ruby, dev jr'
      job_tag.create(0, 'RUBY, Dev JR')
      expect(job_tag.chain_of_tags).to eq tag
    end
  end

  describe 'delete_repeated_tags' do
    let(:job_tag) { described_class.new }

    context 'when tags are repeated' do
      it 'must eliminate repeated ones' do
        tags = 'java,programador,mvc,java,spring,programador'
        filtered_tags = job_tag.delete_repeated_tags(tags)
        expect(filtered_tags).to eq 'java,programador,mvc,spring'
      end
    end

    context 'when dont have repeated tags' do
      it 'must not eliminate tags' do
        tags = 'php,symfony,laravel,mvc'
        filtered_tags = job_tag.delete_repeated_tags(tags)
        expect(filtered_tags).to eq 'php,symfony,laravel,mvc'
      end
    end
  end
end
