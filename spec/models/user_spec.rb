require 'spec_helper'

describe User do
  subject(:user) { described_class.new }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:crypted_password) }
    it { is_expected.to respond_to(:email) }
    it { is_expected.to respond_to(:job_offers) }
  end

  describe 'valid?' do
    it 'should be false when name is blank' do
      user.email = 'john.doe@someplace.com'
      user.password = 'Password123!'

      expect(user.valid?).to eq false
    end

    it 'should be false when email is not valid' do
      user.name = 'John Doe'
      user.email = 'john'
      user.password = 'Password123!'

      expect(user.valid?).to eq false
    end

    it 'should be false when password is blank' do
      user.name = 'John Doe'
      user.email = 'john.doe@someplace.com'

      expect(user.valid?).to eq false
    end

    it 'should be true when all field are valid' do
      user.name = 'John Doe'
      user.email = 'john.doe@someplace.com'
      user.password = 'Password123!'
      user.birthdate = '24/12/1992'

      expect(user.valid?).to eq true
    end
  end

  describe 'authenticate' do
    let(:password) { 'password' }

    before :each do
      user.email = 'john.doe@someplace.com'
      user.password = 'Password123!'
    end

    it 'should return nil when password do not match' do
      email = user.email
      password = 'Wrong_password123!'

      expect(described_class).to receive(:first).with(email: email).and_return(user)

      authentication_result = described_class.authenticate(email, password)

      expect(authentication_result).to be_nil
    end

    it 'should return nil when email do not match' do
      email = 'wrong@email.com'
      expect(described_class).to receive(:first).with(email: email).and_return(nil)

      authentication_result = described_class.authenticate(email, password)

      expect(authentication_result).to be_nil
    end

    it 'should return the user when email and password match' do
      email = user.email
      expect(described_class).to receive(:first).with(email: email).and_return(user)

      authentication_result = described_class.authenticate(email, 'Password123!')

      expect(authentication_result).to eq user
    end
  end

  describe 'strong_password' do
    let(:user) { described_class.new }

    it 'should be return false when introduce password with 7 characters' do
      password = 'Juan12!'

      expect(user.validate_password(password)).to eq false
    end

    it 'should be return false when introduce password without lowercase' do
      password = 'JUAN-+2345'

      expect(user.validate_password(password)).to eq false
    end

    it 'should be return false when introduce password without upperrcase' do
      password = 'juan++2345'

      expect(user.validate_password(password)).to eq false
    end

    it 'should be return false when introduce password without numbers' do
      password = 'JUANholaSi!!'

      expect(user.validate_password(password)).to eq false
    end

    it 'should be return false when introduce password without permitted symbols' do
      password = 'Juan12345'

      expect(user.validate_password(password)).to eq false
    end
  end

  describe 'birthdate' do
    let(:user) { described_class.new }

    it 'should be return true when birthdate is 24/12/1992' do
      birthdate = '24/12/1992'

      expect(user.validate_birthdate(birthdate)).to be_truthy
    end

    it 'should be return false when birthdate is 24/12/2005' do
      birthdate = '24/12/2005'

      expect(user.validate_birthdate(birthdate)).to be_falsey
    end

    it 'should be return false when birthdate is 24/12/1900' do
      birthdate = '24/12/1900'

      expect(user.validate_birthdate(birthdate)).to be_falsey
    end
  end
end
