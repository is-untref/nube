require 'spec_helper'

describe 'JobQuestionController' do
  let(:offer) do
    offer = JobOffer.new
    offer.owner = User.first
    offer.title = 'offer'
    offer.save
  end

  describe 'get :question' do
    it 'should response ok and render job_question/question' do
      get "/job_question/question/#{offer.id}"
      expect(last_response).to be_ok
    end

    it 'should render job_question/question' do
      expect_any_instance_of(JobVacancy::App).to receive(:render).with('job_question/question')
      get "/job_question/question/#{offer.id}"
    end
  end

  describe 'post :question' do
    it 'the page should show the fields must be completed error' do
      post "/job_question/question/#{offer.id}",
           job_question: { email: '' }
      last_response.body.include? 'All fields must be completed!'
    end
    it 'the page should show the invalid email error' do
      post "/job_question/question/#{offer.id}",
           job_question: { email: 'invalid_email', body: 'test', 'subject': 'subject' }
      last_response.body.include? 'The email format is invalid!'
    end
    it 'shoudl render job_question/question_sent' do
      expect_any_instance_of(JobVacancy::App).to receive(:render).with('job_question/question_sent')
      post "/job_question/question/#{offer.id}",
           job_question: { email: 'validmail@valid.com', body: 'test', 'subject': 'subject' }
    end
  end
end
