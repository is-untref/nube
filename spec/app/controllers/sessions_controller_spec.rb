require 'spec_helper'

describe 'SessionsController' do
  describe 'get :' do
    it 'should response ok' do
      expect_any_instance_of(JobVacancy::App).to receive(:render).with('sessions/new')
      get '/login'
      expect(last_response).to be_ok
    end

    it 'should render users/login' do
      expect_any_instance_of(JobVacancy::App).to receive(:render).with('sessions/new')
      get '/login'
    end
  end

  describe 'create' do
    it 'should show Your account is blocked for failed logins! message' do
      expect(FailedLogin).to receive(:is_blocked).and_return(true)
      expect(FailedLogin).to receive(:is_blocked).and_return(true)
      post '/sessions/create',
           user: { email: 'juan@blocked.com', password: 'Juan345!' }
      last_response.body.include? 'The email format is invalid!'
    end
  end
end
